import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Validators } from '@angular/forms';
import { FormBuilder } from '@angular/forms';


@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.sass']
})
export class SignupComponent implements OnInit {

  signupForm = new FormGroup({
    username: new FormControl(''),
    password: new FormControl(''),
  })

  constructor(private fb: FormBuilder) { }

  ngOnInit() {
  }

  signUpUser = this.fb.group({
    userName: ['', Validators.required],
    password: ['', Validators.required]
  })

  onsubmit(){
    console.warn(this.signupForm.value);
  }
}
